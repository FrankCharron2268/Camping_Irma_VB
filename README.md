To Download: Click the cloud-looking button on the right of the screen

To launch project:
- Download or Pull Project.
- Open Camping_VB_Build.
- Launch Irmacamping.exe.

To view solution:
- Download or Pull Project.
- Open Camping_VB_Solution -> mi-session. 
- Open mi-session.sln in your IDE or double click it.