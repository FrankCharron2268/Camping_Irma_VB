﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class canoeescalade
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(canoeescalade))
        Me.back = New System.Windows.Forms.Button()
        Me.rbweek1 = New System.Windows.Forms.RadioButton()
        Me.rbwd1 = New System.Windows.Forms.RadioButton()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.timecount = New System.Windows.Forms.NumericUpDown()
        Me.boatcount = New System.Windows.Forms.NumericUpDown()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.timecount2 = New System.Windows.Forms.NumericUpDown()
        Me.pplcountesc = New System.Windows.Forms.NumericUpDown()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        CType(Me.timecount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.boatcount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.timecount2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pplcountesc, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'back
        '
        Me.back.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.back.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.back.Font = New System.Drawing.Font("Palatino Linotype", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle))
        Me.back.Location = New System.Drawing.Point(360, 497)
        Me.back.Name = "back"
        Me.back.Size = New System.Drawing.Size(148, 40)
        Me.back.TabIndex = 26
        Me.back.Text = "Comfirmez"
        Me.back.UseVisualStyleBackColor = False
        '
        'rbweek1
        '
        Me.rbweek1.AutoSize = True
        Me.rbweek1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.rbweek1.Font = New System.Drawing.Font("Palatino Linotype", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle))
        Me.rbweek1.Location = New System.Drawing.Point(15, 508)
        Me.rbweek1.Name = "rbweek1"
        Me.rbweek1.Size = New System.Drawing.Size(115, 22)
        Me.rbweek1.TabIndex = 25
        Me.rbweek1.TabStop = True
        Me.rbweek1.Text = "Jour de semaine"
        Me.rbweek1.UseVisualStyleBackColor = False
        '
        'rbwd1
        '
        Me.rbwd1.AutoSize = True
        Me.rbwd1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.rbwd1.Font = New System.Drawing.Font("Palatino Linotype", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle))
        Me.rbwd1.Location = New System.Drawing.Point(150, 508)
        Me.rbwd1.Name = "rbwd1"
        Me.rbwd1.Size = New System.Drawing.Size(204, 22)
        Me.rbwd1.TabIndex = 24
        Me.rbwd1.TabStop = True
        Me.rbwd1.Text = "Fin de semaine ou jour de congé"
        Me.rbwd1.UseVisualStyleBackColor = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label4.Font = New System.Drawing.Font("Palatino Linotype", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle))
        Me.Label4.Location = New System.Drawing.Point(45, 131)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(110, 18)
        Me.Label4.TabIndex = 23
        Me.Label4.Text = "Temps de location"
        '
        'timecount
        '
        Me.timecount.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.timecount.Location = New System.Drawing.Point(173, 129)
        Me.timecount.Name = "timecount"
        Me.timecount.Size = New System.Drawing.Size(37, 20)
        Me.timecount.TabIndex = 22
        Me.timecount.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'boatcount
        '
        Me.boatcount.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.boatcount.Location = New System.Drawing.Point(173, 103)
        Me.boatcount.Name = "boatcount"
        Me.boatcount.Size = New System.Drawing.Size(37, 20)
        Me.boatcount.TabIndex = 21
        Me.boatcount.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label3.Font = New System.Drawing.Font("Palatino Linotype", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle))
        Me.Label3.Location = New System.Drawing.Point(12, 103)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(143, 18)
        Me.Label3.TabIndex = 20
        Me.Label3.Text = "Nombre d'embarcations"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label2.Font = New System.Drawing.Font("Palatino Linotype", 10.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle))
        Me.Label2.Location = New System.Drawing.Point(0, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Padding = New System.Windows.Forms.Padding(5)
        Me.Label2.Size = New System.Drawing.Size(243, 86)
        Me.Label2.TabIndex = 19
        Me.Label2.Text = "Tarifs de location de canoe" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Par embarquation, bloc de 2 heures." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "   Semaine     " &
    "   Fin de semaine" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "$22,35                 $29,55" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label5.Font = New System.Drawing.Font("Palatino Linotype", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle))
        Me.Label5.Location = New System.Drawing.Point(45, 268)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(110, 18)
        Me.Label5.TabIndex = 30
        Me.Label5.Text = "Temps de location"
        '
        'timecount2
        '
        Me.timecount2.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.timecount2.Location = New System.Drawing.Point(177, 266)
        Me.timecount2.Name = "timecount2"
        Me.timecount2.Size = New System.Drawing.Size(37, 20)
        Me.timecount2.TabIndex = 29
        Me.timecount2.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'pplcountesc
        '
        Me.pplcountesc.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.pplcountesc.Location = New System.Drawing.Point(177, 240)
        Me.pplcountesc.Name = "pplcountesc"
        Me.pplcountesc.Size = New System.Drawing.Size(37, 20)
        Me.pplcountesc.TabIndex = 28
        Me.pplcountesc.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label6.Font = New System.Drawing.Font("Palatino Linotype", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle))
        Me.Label6.Location = New System.Drawing.Point(26, 242)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(129, 18)
        Me.Label6.TabIndex = 27
        Me.Label6.Text = "Nombre de personnes"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label7.Font = New System.Drawing.Font("Palatino Linotype", 10.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle))
        Me.Label7.ForeColor = System.Drawing.Color.Black
        Me.Label7.Location = New System.Drawing.Point(-6, 177)
        Me.Label7.Name = "Label7"
        Me.Label7.Padding = New System.Windows.Forms.Padding(5)
        Me.Label7.Size = New System.Drawing.Size(308, 48)
        Me.Label7.TabIndex = 34
        Me.Label7.Text = "Location d'un casque, d'un piolet et de cordes:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "10$ par personne"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'canoeescalade
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(763, 547)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.timecount2)
        Me.Controls.Add(Me.pplcountesc)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.back)
        Me.Controls.Add(Me.rbweek1)
        Me.Controls.Add(Me.rbwd1)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.timecount)
        Me.Controls.Add(Me.boatcount)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Name = "canoeescalade"
        Me.Text = "Form3"
        CType(Me.timecount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.boatcount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.timecount2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pplcountesc, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents back As Button
    Friend WithEvents rbweek1 As RadioButton
    Friend WithEvents rbwd1 As RadioButton
    Friend WithEvents Label4 As Label
    Friend WithEvents timecount As NumericUpDown
    Friend WithEvents boatcount As NumericUpDown
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents timecount2 As NumericUpDown
    Friend WithEvents pplcountesc As NumericUpDown
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
End Class
