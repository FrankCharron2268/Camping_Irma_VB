﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class equitation
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(equitation))
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.pplcountequi = New System.Windows.Forms.NumericUpDown()
        Me.trackchoice = New System.Windows.Forms.NumericUpDown()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.rbwd = New System.Windows.Forms.RadioButton()
        Me.back = New System.Windows.Forms.Button()
        Me.rbweek = New System.Windows.Forms.RadioButton()
        CType(Me.pplcountequi, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.trackchoice, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.SandyBrown
        Me.Label2.Font = New System.Drawing.Font("Palatino Linotype", 10.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle))
        Me.Label2.Location = New System.Drawing.Point(0, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Padding = New System.Windows.Forms.Padding(5)
        Me.Label2.Size = New System.Drawing.Size(287, 105)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = resources.GetString("Label2.Text")
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.SandyBrown
        Me.Label3.Font = New System.Drawing.Font("Palatino Linotype", 10.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle))
        Me.Label3.Location = New System.Drawing.Point(12, 123)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(142, 19)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Nombre de personnes"
        '
        'pplcountequi
        '
        Me.pplcountequi.BackColor = System.Drawing.Color.SandyBrown
        Me.pplcountequi.Location = New System.Drawing.Point(160, 122)
        Me.pplcountequi.Name = "pplcountequi"
        Me.pplcountequi.Size = New System.Drawing.Size(37, 20)
        Me.pplcountequi.TabIndex = 3
        Me.pplcountequi.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'trackchoice
        '
        Me.trackchoice.BackColor = System.Drawing.Color.SandyBrown
        Me.trackchoice.Location = New System.Drawing.Point(160, 161)
        Me.trackchoice.Name = "trackchoice"
        Me.trackchoice.Size = New System.Drawing.Size(37, 20)
        Me.trackchoice.TabIndex = 3
        Me.trackchoice.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.SandyBrown
        Me.Label4.Font = New System.Drawing.Font("Palatino Linotype", 10.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle))
        Me.Label4.Location = New System.Drawing.Point(30, 160)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(124, 19)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Choix du parcours"
        '
        'rbwd
        '
        Me.rbwd.AutoSize = True
        Me.rbwd.BackColor = System.Drawing.Color.SandyBrown
        Me.rbwd.Font = New System.Drawing.Font("Palatino Linotype", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle))
        Me.rbwd.Location = New System.Drawing.Point(150, 508)
        Me.rbwd.Name = "rbwd"
        Me.rbwd.Size = New System.Drawing.Size(204, 22)
        Me.rbwd.TabIndex = 6
        Me.rbwd.TabStop = True
        Me.rbwd.Text = "Fin de semaine ou jour de congé"
        Me.rbwd.UseVisualStyleBackColor = False
        '
        'back
        '
        Me.back.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.back.BackColor = System.Drawing.Color.SandyBrown
        Me.back.Font = New System.Drawing.Font("Palatino Linotype", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle))
        Me.back.Location = New System.Drawing.Point(360, 497)
        Me.back.Name = "back"
        Me.back.Size = New System.Drawing.Size(148, 40)
        Me.back.TabIndex = 8
        Me.back.Text = "Comfirmez"
        Me.back.UseVisualStyleBackColor = False
        '
        'rbweek
        '
        Me.rbweek.AutoSize = True
        Me.rbweek.BackColor = System.Drawing.Color.SandyBrown
        Me.rbweek.Font = New System.Drawing.Font("Palatino Linotype", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle))
        Me.rbweek.Location = New System.Drawing.Point(16, 508)
        Me.rbweek.Name = "rbweek"
        Me.rbweek.Size = New System.Drawing.Size(115, 22)
        Me.rbweek.TabIndex = 7
        Me.rbweek.TabStop = True
        Me.rbweek.Text = "Jour de semaine"
        Me.rbweek.UseVisualStyleBackColor = False
        '
        'equitation
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.SaddleBrown
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(763, 547)
        Me.Controls.Add(Me.back)
        Me.Controls.Add(Me.rbweek)
        Me.Controls.Add(Me.rbwd)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.trackchoice)
        Me.Controls.Add(Me.pplcountequi)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Name = "equitation"
        Me.Text = "Form2"
        CType(Me.pplcountequi, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.trackchoice, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents pplcountequi As NumericUpDown
    Friend WithEvents trackchoice As NumericUpDown
    Friend WithEvents Label4 As Label
    Friend WithEvents rbwd As RadioButton
    Friend WithEvents back As Button
    Friend WithEvents rbweek As RadioButton
End Class
