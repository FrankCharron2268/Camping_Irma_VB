﻿
Public Class main
    Private Sub equibutton_Click(sender As Object, e As EventArgs) Handles gotoequi.Click
        equitation.Show()
        Me.Hide()
    End Sub

    Private Sub gotocanot_Click(sender As Object, e As EventArgs) Handles gotocanot.Click
        canoeescalade.Show()
        Me.Hide()
    End Sub


    Public Sub calculate_Click(sender As Object, e As EventArgs) Handles calculate.Click


        REM calcul equitation

        Dim PplcountEqui As Integer
        Dim choix As Integer
        Dim PrixEqui As Decimal

        PplcountEqui = CInt(equitation.pplcountequi.Value)
        choix = CInt(equitation.trackchoice.Value)

        If equitation.rbweek.Checked Then
            If choix = 0 Then PrixEqui = CDec(0.00)
            If choix = 1 Then PrixEqui = CDec(15.25)
            If choix = 2 Then PrixEqui = CDec(22.75)
            If choix = 3 Then PrixEqui = CDec(25.25)
        ElseIf equitation.rbwd.Checked Then
            If choix = 0 Then PrixEqui = CDec(0.00)
            If choix = 1 Then PrixEqui = CDec(18.25)
            If choix = 2 Then PrixEqui = CDec(25.0)
            If choix = 3 Then PrixEqui = CDec(27.75)
        End If
        REM calcul equitation


        REM calcul escalade
        Dim PplcountEsc As Integer
        Dim TimecountEsc As Integer
        Dim PrixEsc As Decimal

        PplcountEsc = CInt(canoeescalade.pplcountesc.Value)
        TimecountEsc = CInt(canoeescalade.timecount2.Value)
        PrixEsc = 10

        REM calcul escalade

        REM calcul canot
        Dim BoatCount As Integer
        Dim TimecountCanot As Integer
        Dim PrixCanot As Decimal

        BoatCount = CInt(canoeescalade.boatcount.Value)
        TimecountCanot = CInt(canoeescalade.timecount.Value)
        If canoeescalade.rbweek1.Checked Then PrixCanot = CDec(22.35)
        If canoeescalade.rbwd1.Checked Then PrixCanot = CDec(29.55)

        REM calcul canot

        REM calcule sejour
        Dim MinDate1 As Date = New Date(2016, 5, 1)
        Dim MaxDate1 As Date = New Date(2016, 5, 31)
        Dim MinDate2 As Date = New Date(2016, 6, 1)
        Dim MaxDate2 As Date = New Date(2016, 8, 31)
        Dim MinDate3 As Date = New Date(2016, 9, 1)
        Dim MaxDate3 As Date = New Date(2016, 9, 30)
        Dim PrixSejour As Decimal
        Dim Total As Double
        Dim PplcountSejour As Integer
        PplcountSejour = CInt(pplcount.Value)

        Dim StartDate As DateTime
        Dim EndDate As DateTime

        StartDate = datepick1.Value
        EndDate = datepick2.Value

        If StartDate >= MinDate1 And EndDate <= MaxDate1 Then
            PrixSejour = CDec(18.91)
        ElseIf StartDate >= MinDate2 And EndDate <= MaxDate2 Then
            PrixSejour = CDec(23.25)
        ElseIf StartDate >= MinDate3 And EndDate <= MaxDate3 Then
            PrixSejour = CDec(20.25)


        End If
        REM calcul sejour

        REM misc 
        detailprix.Visible = True
        Dim Nom As String
        Nom = nametxt.Text

        REM misc


        REM calcul  Total
        Total = FraisSejour(PrixSejour, PplcountSejour, StartDate, EndDate) + FraisEquitation(PrixEqui, PplcountEqui) + FraisEscalade(PrixEsc, TimecountEsc, PplcountEsc) +
            FraisCanot(PrixCanot, BoatCount, TimecountCanot) + CalculTaxe(Total)

        detailprix.Text = "Merci, " + Nom + "." + vbNewLine + "Voici les details de votre selection:" + vbNewLine + "Sejour           :" +
            CStr(Format(FraisSejour(PrixSejour, PplcountSejour, StartDate, EndDate), "C")) + vbNewLine + "Equitation  :" +
            CStr(Format(FraisEquitation(PrixEqui, PplcountEqui), "c")) + vbNewLine + "Escalade     :" +
            CStr(Format(FraisEscalade(PrixEsc, TimecountEsc, PplcountEsc), "C")) + vbNewLine + "Canot          :" +
            CStr(Format(FraisCanot(PrixCanot, BoatCount, TimecountCanot), "c")) + vbNewLine + vbNewLine + "Total            :" +
            CStr(Format(Total, "c")) + vbNewLine + "Taxe Total : " + CStr(Format(CalculTaxe(Total), "c"))
        REM calcul Total
    End Sub



    Public Function FraisCanot(prixbase As Decimal, TimecountCanot As Integer, BoatCount As Integer) As Double

        Return prixbase * TimecountCanot * BoatCount

    End Function



    Public Function FraisEscalade(PrixEsc As Double, PplcountEsc As Integer, TimecountEsc As Integer) As Double
        Return (PrixEsc * PplcountEsc) * TimecountEsc

    End Function

    Public Function FraisEquitation(PrixEqui As Double, PplcountEqui As Integer) As Double
        Return PrixEqui * PplcountEqui

    End Function

    Public Function FraisSejour(PrixSejour As Double, PplcountSejour As Integer, StartDate As Date, EndDate As Date) As Double
        Return (PrixSejour * PplcountSejour) * DateDiff(DateInterval.DayOfYear, StartDate, EndDate)

    End Function
    Public Function CalculTaxe(total As Double) As Double
        Return total * (14.975 / 100)
    End Function
    Public Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        detailprix.Clear()
        detailprix.Visible = False
        nametxt.Clear()
        nametxt.Focus()

        For Each ctrl As Control In Me.Controls
            If TypeOf ctrl Is NumericUpDown Then
                DirectCast(ctrl, NumericUpDown).Value = 0
            End If
        Next

        For Each ctrl As Control In equitation.Controls
            If TypeOf ctrl Is NumericUpDown Then
                DirectCast(ctrl, NumericUpDown).Value = 0
            End If
        Next

        For Each ctrl As Control In canoeescalade.Controls
            If TypeOf ctrl Is NumericUpDown Then

                DirectCast(ctrl, NumericUpDown).Value = 0
            End If
        Next
    End Sub


End Class
