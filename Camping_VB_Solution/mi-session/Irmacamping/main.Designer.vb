﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class main
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(main))
        Me.datepick1 = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.datepick2 = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.nametxt = New System.Windows.Forms.TextBox()
        Me.pplcount = New System.Windows.Forms.NumericUpDown()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.gotoequi = New System.Windows.Forms.Button()
        Me.gotocanot = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.calculate = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.detailprix = New System.Windows.Forms.RichTextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        CType(Me.pplcount, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'datepick1
        '
        Me.datepick1.Font = New System.Drawing.Font("Palatino Linotype", 10.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle))
        Me.datepick1.Location = New System.Drawing.Point(126, 175)
        Me.datepick1.Name = "datepick1"
        Me.datepick1.Size = New System.Drawing.Size(200, 25)
        Me.datepick1.TabIndex = 0
        Me.datepick1.Value = New Date(2016, 5, 10, 18, 26, 0, 0)
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Green
        Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label1.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Label1.Font = New System.Drawing.Font("Palatino Linotype", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle))
        Me.Label1.Location = New System.Drawing.Point(25, 180)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(85, 20)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Date d'arrivé"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Green
        Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label2.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Label2.Font = New System.Drawing.Font("Palatino Linotype", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle))
        Me.Label2.Location = New System.Drawing.Point(25, 216)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(94, 20)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Date de départ"
        '
        'datepick2
        '
        Me.datepick2.Font = New System.Drawing.Font("Palatino Linotype", 10.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle))
        Me.datepick2.Location = New System.Drawing.Point(126, 211)
        Me.datepick2.Name = "datepick2"
        Me.datepick2.Size = New System.Drawing.Size(200, 25)
        Me.datepick2.TabIndex = 4
        Me.datepick2.Value = New Date(2016, 5, 13, 18, 26, 0, 0)
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Green
        Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label3.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Label3.Font = New System.Drawing.Font("Palatino Linotype", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle))
        Me.Label3.Location = New System.Drawing.Point(25, 107)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(71, 20)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Au nom de:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Green
        Me.Label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label4.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Label4.Font = New System.Drawing.Font("Palatino Linotype", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle))
        Me.Label4.Location = New System.Drawing.Point(25, 145)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(131, 20)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Nombre de personnes"
        '
        'nametxt
        '
        Me.nametxt.Font = New System.Drawing.Font("Palatino Linotype", 10.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle))
        Me.nametxt.Location = New System.Drawing.Point(126, 107)
        Me.nametxt.Name = "nametxt"
        Me.nametxt.Size = New System.Drawing.Size(200, 25)
        Me.nametxt.TabIndex = 7
        '
        'pplcount
        '
        Me.pplcount.Font = New System.Drawing.Font("Palatino Linotype", 10.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle))
        Me.pplcount.Location = New System.Drawing.Point(167, 143)
        Me.pplcount.Name = "pplcount"
        Me.pplcount.Size = New System.Drawing.Size(37, 25)
        Me.pplcount.TabIndex = 8
        Me.pplcount.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Green
        Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label5.Font = New System.Drawing.Font("Palatino Linotype", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(50, 462)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(76, 23)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Activités"
        '
        'gotoequi
        '
        Me.gotoequi.BackColor = System.Drawing.Color.Green
        Me.gotoequi.Font = New System.Drawing.Font("Palatino Linotype", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gotoequi.Location = New System.Drawing.Point(50, 505)
        Me.gotoequi.Name = "gotoequi"
        Me.gotoequi.Size = New System.Drawing.Size(106, 32)
        Me.gotoequi.TabIndex = 10
        Me.gotoequi.Text = "Equitation"
        Me.gotoequi.UseVisualStyleBackColor = False
        '
        'gotocanot
        '
        Me.gotocanot.AutoSize = True
        Me.gotocanot.BackColor = System.Drawing.Color.Green
        Me.gotocanot.Font = New System.Drawing.Font("Palatino Linotype", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gotocanot.Location = New System.Drawing.Point(167, 505)
        Me.gotocanot.Name = "gotocanot"
        Me.gotocanot.Size = New System.Drawing.Size(150, 32)
        Me.gotocanot.TabIndex = 11
        Me.gotocanot.Text = "Canot et Escalade"
        Me.gotocanot.UseVisualStyleBackColor = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.ForestGreen
        Me.Label6.Font = New System.Drawing.Font("Papyrus", 18.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Black
        Me.Label6.Location = New System.Drawing.Point(455, 21)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(281, 44)
        Me.Label6.TabIndex = 13
        Me.Label6.Text = "Le camping de tante Irma"
        Me.Label6.UseCompatibleTextRendering = True
        '
        'calculate
        '
        Me.calculate.BackColor = System.Drawing.Color.Green
        Me.calculate.Font = New System.Drawing.Font("Palatino Linotype", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.calculate.ImageAlign = System.Drawing.ContentAlignment.BottomLeft
        Me.calculate.Location = New System.Drawing.Point(360, 497)
        Me.calculate.Name = "calculate"
        Me.calculate.Size = New System.Drawing.Size(148, 40)
        Me.calculate.TabIndex = 14
        Me.calculate.Text = "Tarif"
        Me.calculate.UseVisualStyleBackColor = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.ForestGreen
        Me.Label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label7.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Label7.Font = New System.Drawing.Font("Palatino Linotype", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.Black
        Me.Label7.Location = New System.Drawing.Point(0, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Padding = New System.Windows.Forms.Padding(5)
        Me.Label7.Size = New System.Drawing.Size(416, 84)
        Me.Label7.TabIndex = 15
        Me.Label7.Text = resources.GetString("Label7.Text")
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'detailprix
        '
        Me.detailprix.BackColor = System.Drawing.Color.ForestGreen
        Me.detailprix.Font = New System.Drawing.Font("Palatino Linotype", 10.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle))
        Me.detailprix.Location = New System.Drawing.Point(16, 242)
        Me.detailprix.Name = "detailprix"
        Me.detailprix.Size = New System.Drawing.Size(239, 198)
        Me.detailprix.TabIndex = 17
        Me.detailprix.Text = ""
        Me.detailprix.Visible = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Green
        Me.Button1.Font = New System.Drawing.Font("Palatino Linotype", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.BottomLeft
        Me.Button1.Location = New System.Drawing.Point(514, 497)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(148, 40)
        Me.Button1.TabIndex = 18
        Me.Button1.Text = "Nouveau Client"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'main
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(763, 547)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.detailprix)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.calculate)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.gotocanot)
        Me.Controls.Add(Me.gotoequi)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.pplcount)
        Me.Controls.Add(Me.nametxt)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.datepick2)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.datepick1)
        Me.Name = "main"
        CType(Me.pplcount, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents datepick1 As DateTimePicker
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents datepick2 As DateTimePicker
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents nametxt As TextBox
    Friend WithEvents pplcount As NumericUpDown
    Friend WithEvents Label5 As Label
    Friend WithEvents gotoequi As Button
    Friend WithEvents gotocanot As Button
    Friend WithEvents Label6 As Label
    Friend WithEvents calculate As Button
    Friend WithEvents Label7 As Label
    Friend WithEvents detailprix As RichTextBox
    Friend WithEvents Button1 As Button
End Class
